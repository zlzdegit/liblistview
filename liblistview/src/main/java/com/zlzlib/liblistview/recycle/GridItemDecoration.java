package com.zlzlib.liblistview.recycle;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @Desc: 网格间隔
 * @Copyright:
 * @DateTime: 2021/11/5 12:05
 * @Author zlz
 * @Version 1.0
 */
public class GridItemDecoration extends RecyclerView.ItemDecoration {
    //间隔
    private final int space;
    //每行个数
    private final int count;

    public GridItemDecoration(int space, int count) {
        this.space = space;
        this.count = count;
    }

    @Override
    public void getItemOffsets(Rect outRect, @NonNull View view, RecyclerView parent, @NonNull RecyclerView.State state) {
        //设置底部
        outRect.bottom = space;
        //由于每行都只有count个，每行最后一个设置为0
        if (parent.getChildLayoutPosition(view) % count == count - 1) {
            outRect.right = 0;
        } else {
            outRect.right = space;
        }
    }
}
