package com.zlzlib.liblistview.recycle;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @Desc: FlowLayoutManager 流式布局使用的时候有问题就用这个
 * @Copyright:
 * @DateTime: 2020/1/12 11:35
 * @Author zlz
 * @Version 1.0
 */
public class FlowRecycleView extends RecyclerView {

    public FlowRecycleView(Context context) {
        super(context);
    }

    public FlowRecycleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public FlowRecycleView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        super.onMeasure(widthSpec, heightSpec);
        if (getLayoutManager() instanceof FlowLayoutManager) {
            FlowLayoutManager layoutManager = (FlowLayoutManager) getLayoutManager();
            int widthMode = MeasureSpec.getMode(widthSpec);
            int measureWidth = MeasureSpec.getSize(widthSpec);
            int heightMode = MeasureSpec.getMode(heightSpec);
            int measureHeight = MeasureSpec.getSize(heightSpec);
            int width, height;
            if (widthMode == MeasureSpec.EXACTLY) {
                width = measureWidth;
            } else {
                //以实际屏宽为标准
                width = getContext().getResources().getDisplayMetrics().widthPixels;
            }
            if (heightMode == MeasureSpec.EXACTLY) {
                height = measureHeight;
            } else {
                height = layoutManager.getTotalHeight() + getPaddingTop() + getPaddingBottom();
            }
            setMeasuredDimension(width, height);
        }
    }

}
