package com.zlzlib.liblistview.adapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @Desc: 可选择列表
 * @Copyright:
 * @DateTime: 2022/3/22 17:14
 * @Author zlz
 * @Version 1.0
 */
public abstract class SelectRecAdapter<T extends SelectFace> extends BaseRecAdapter<T> {

    private final List<T> selectList = new ArrayList<>();

    public void cleanSelect() {
        for (T t : selectList) {
            t.setSelect(false);
        }
        selectList.clear();
        notifyDataSetChanged();
    }

    @Override
    public void refreshData(List<T> list) {
        super.refreshData(list);
        selectList.clear();
        for (T t : list) {
            if (t.isSelect()) {
                selectList.add(t);
            }
        }
    }

    @Override
    public void cleanData() {
        super.cleanData();
        selectList.clear();
    }

    @Override
    public void addAllData(int startIndex, List<T> list) {
        for (T t : list) {
            if (t.isSelect()) {
                selectList.add(t);
            }
        }
        super.addAllData(startIndex, list);
    }

    @Override
    public void addData(T bean) {
        addData(bean, getData().size());
    }

    @Override
    public void addData(T bean, int pos) {
        if (bean.isSelect()) {
            selectList.add(bean);
        }
        super.addData(bean, pos);
    }

    @Override
    public void removeData(T bean) {
        int pos = getData().indexOf(bean);
        removeData(pos);
    }

    @Override
    public void removeData(int pos) {
        T bean = getData().get(pos);
        selectList.remove(bean);
        super.removeData(pos);
    }

    @Override
    public void removeDataList(int startIndex, int count) {
        for (int i = startIndex; i < startIndex + count; i++) {
            if (i < getData().size()) {
                selectList.remove(getData().get(i));
            }
        }
        super.removeDataList(startIndex, count);
    }

    @Override
    public void notifyData(T bean) {
        if (bean.isSelect()) {
            if (!selectList.contains(bean)) {
                selectList.add(bean);
            }
        } else {
            selectList.remove(bean);
        }
        super.notifyData(bean);
    }

    /**
     * @param bean 单选选择的数据
     */
    public void selectSingleItem(T bean) {
        List<T> list = new ArrayList<>(1);
        list.add(bean);
        selectSingleItem(list);
    }

    /**
     * @param list 单选选择的列表
     */
    public void selectSingleItem(List<T> list) {
        List<T> tmpList = new ArrayList<>();
        for (T v : selectList) {
            v.setSelect(false);
            tmpList.add(v);
        }
        for (T t : tmpList) {
            notifyData(t);
        }
        for (T v : list) {
            v.setSelect(true);
            notifyData(v);
        }
    }

    /**
     * @param pos 单选选择的下标
     */
    public void selectSingleItem(int pos) {
        selectSingleItem(getItem(pos));
    }

    /**
     * @param bean 多选选中的对象
     */
    public void selectItem(T bean) {
        List<T> list = new ArrayList<>(1);
        list.add(bean);
        selectItem(list);
    }

    /**
     * @param list 多选选中的列表
     */
    public void selectItem(List<T> list) {
        for (T t : list) {
            t.setSelect(!t.isSelect());
            notifyData(t);
        }
    }

    /**
     * @param pos 多选选中的下标
     */
    public void selectItem(int pos) {
        selectItem(getItem(pos));
    }

    /**
     * @return 当前选中的列表
     */
    public List<T> getSelectList() {
        return selectList;
    }

    /**
     * @return 当前单选选中的对象
     */
    public T getSelectSingle() {
        if (selectList.isEmpty()) {
            return null;
        }
        return selectList.get(0);
    }
}
