package com.zlzlib.liblistview.adapter;

/**
 * @Desc:
 * @Copyright:
 * @DateTime: 2022/4/7 16:23
 * @Author zlz
 * @Version 1.0
 */
public class ExpandBean implements ExpandFace {

    /**
     * 是否展开
     */
    protected boolean isExpand = false;

    @Override
    public boolean isExpand() {
        return isExpand;
    }

    @Override
    public void setExpand(boolean isExpand) {
        this.isExpand = isExpand;
    }
}
