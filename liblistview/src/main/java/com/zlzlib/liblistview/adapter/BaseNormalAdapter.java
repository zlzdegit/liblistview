package com.zlzlib.liblistview.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zlz on 2019/10/29.
 */
public abstract class BaseNormalAdapter<T> extends BaseAdapter {

    protected List<T> data;
    protected Context context;

    public BaseNormalAdapter() {
        data = new ArrayList<>();
    }

    public BaseNormalAdapter(List<T> data) {
        this.data = data;
    }

    protected abstract int layoutId();

    //返回当前布局绑定的对象id
    protected abstract int getVariableId();

    //返回当前布局绑定的对象
    protected Object getVariableBean(int pos) {
        return getItem(pos);
    }

    //绑定数据
    protected abstract void bindData(BaseHolder holder, T bean);

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public T getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BaseHolder holder;
        if (convertView == null) {
            LayoutInflater from = LayoutInflater.from(context = parent.getContext());
            ViewDataBinding inflate = DataBindingUtil.inflate(from, layoutId(), parent, false);//引入布局
            convertView = inflate.getRoot();
            holder = new BaseHolder(inflate);
            convertView.setTag(holder);
        } else {
            holder = (BaseHolder) convertView.getTag();
        }
        if (getVariableBean(position) != null) {
            holder.getDataBinding().setVariable(getVariableId(), getVariableBean(position));
        }
        holder.setPos(position);
        bindData(holder, getItem(position));
        return convertView;
    }

    public static class BaseHolder {
        private int pos;
        private final ViewDataBinding dataBinding;

        public BaseHolder(ViewDataBinding dataBinding) {
            this.dataBinding = dataBinding;
        }

        public int getPos() {
            return pos;
        }

        public void setPos(int pos) {
            this.pos = pos;
        }

        public <B extends ViewDataBinding> B getDataBinding() {
            return (B) dataBinding;
        }
    }

    public List<T> getData() {
        return data;
    }

    public void refreshData(List<T> list) {
        data.clear();
        data.addAll(list);
        notifyDataSetChanged();
    }

    public void cleanData() {
        data.clear();
        notifyDataSetChanged();
    }

    public void addDataAll(List<T> list) {
        data.addAll(list);
        notifyDataSetChanged();
    }

    public void addData(T bean) {
        data.add(bean);
        notifyDataSetChanged();
    }

    public void addData(T bean, int pos) {
        data.add(pos, bean);
        notifyDataSetChanged();
    }

    public void removeData(T bean) {
        data.remove(bean);
        notifyDataSetChanged();
    }

    public void removeData(int pos) {
        data.remove(pos);
        notifyDataSetChanged();
    }

}
