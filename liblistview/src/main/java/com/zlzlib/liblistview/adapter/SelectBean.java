package com.zlzlib.liblistview.adapter;

/**
 * @Desc:
 * @Copyright:
 * @DateTime: 2022/4/7 16:23
 * @Author zlz
 * @Version 1.0
 */
public class SelectBean implements SelectFace {

    /**
     * 是否选中
     */
    protected boolean isSelect = false;

    @Override
    public boolean isSelect() {
        return isSelect;
    }

    @Override
    public void setSelect(boolean select) {
        isSelect = select;
    }
}
