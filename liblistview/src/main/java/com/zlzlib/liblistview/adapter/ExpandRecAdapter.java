package com.zlzlib.liblistview.adapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @Desc: 可以展开的rec的adapter
 * @Copyright:
 * @DateTime: 2020/9/3 9:34
 * @Author zlz
 * @Version 1.0
 */
public abstract class ExpandRecAdapter<T extends ExpandFace> extends BaseRecAdapter<T> {

    private final List<T> dataAll;

    public ExpandRecAdapter() {
        dataAll = new ArrayList<>();
    }

    /**
     * @param list 初始化列表所有的数据
     */
    public void initAllData(List<T> list) {
        dataAll.clear();
        dataAll.addAll(list);
    }

    /**
     * @return 获得列表所有数据
     */
    public List<T> getDataAll() {
        return dataAll;
    }

    @Override
    public void cleanData() {
        super.cleanData();
        dataAll.clear();
    }

    /**
     * @param pos 下标
     * @return 当前选择的父类的子类
     */
    protected abstract List<T> getChildList(int pos);

    /**
     * 点击展开和收拢
     *
     * @param pos 点击的下标
     */
    public void parentClick(int pos) {
        T bean = getItem(pos);
        List<T> child = getChildList(pos);
        //子类或者没有子类
        if (child == null || getChildList(pos).isEmpty()) {
            bean.setExpand(!bean.isExpand());
            notifyItemChanged(pos);
            return;
        }
        if (bean.isExpand()) {
            //收拢  移除
            removeDataList(pos + 1, child.size());
        } else {
            //展开  添加
            addAllData(pos + 1, child);
        }
        bean.setExpand(!bean.isExpand());
        notifyItemChanged(pos);
    }

}
