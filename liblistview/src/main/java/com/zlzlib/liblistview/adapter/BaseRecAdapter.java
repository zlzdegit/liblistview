package com.zlzlib.liblistview.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by zlz on 2019/9/29.
 */
public abstract class BaseRecAdapter<T> extends RecyclerView.Adapter<BaseRecAdapter.BaseHolder> {

    private static class ViewClickDelay implements View.OnClickListener {

        private final ViewClickDelayListener viewClickDelayListener;
        //上一次点击时间
        private long lastClickTime = 0;

        public ViewClickDelay(ViewClickDelayListener viewClickDelayListener) {
            this.viewClickDelayListener = viewClickDelayListener;
        }

        @Override
        public void onClick(View v) {
            long currentTime = System.currentTimeMillis();
            //最小点击间隔时间
            long minDelayTime = 1000;
            if (currentTime - lastClickTime > minDelayTime) {
                lastClickTime = currentTime;
                viewClickDelayListener.clickDelay(v);
            }
        }

        public interface ViewClickDelayListener {
            void clickDelay(View v);
        }
    }

    private final List<T> data;
    private OnItemClick itemClick;
    private OnItemLongClick longClick;
    protected Context context;

    public BaseRecAdapter() {
        this.data = new ArrayList<>();
    }

    public BaseRecAdapter(List<T> data) {
        this.data = data;
    }

    public void setItemClick(OnItemClick itemClick) {
        this.itemClick = itemClick;
    }

    public void setLongClick(OnItemLongClick longClick) {
        this.longClick = longClick;
    }

    protected abstract int getLayoutId(int pos);

    //返回当前布局绑定的对象id
    protected abstract int getVariableId();

    //返回当前布局绑定的对象
    protected Object getVariableBean(int pos) {
        return getItem(pos);
    }

    //绑定数据当前条目
    protected void bindMyData(T bean, BaseHolder holder) {
    }

    @NonNull
    @Override
    public BaseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater from = LayoutInflater.from(context = parent.getContext());
        ViewDataBinding inflate = DataBindingUtil.inflate(from, viewType, parent, false);//引入布局
        return new BaseHolder(inflate);
    }


    @Override
    public void onBindViewHolder(@NonNull final BaseRecAdapter.BaseHolder holder, int position) {
        //设置条目点击事件
        if (null != itemClick) {
            holder.getDataBinding().getRoot().setOnClickListener(new ViewClickDelay(v ->
                    itemClick.itemClick(v, holder.getLayoutPosition())));
        }
        if (null != longClick) {
            holder.getDataBinding().getRoot().setOnLongClickListener(view ->
                    longClick.itemClick(view, holder.getLayoutPosition()));
        }
        Object bean = getVariableBean(position);
        if(bean!=null){
            holder.getDataBinding().setVariable(getVariableId(), bean);
        }
        bindMyData(getItem(position), holder);
    }

    @Override
    public int getItemViewType(int position) {
        //这里直接返回不同类型item 的布局id
        return getLayoutId(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    //获得每一条的数据
    public T getItem(int pos) {
        return data.get(pos);
    }

    public List<T> getData() {
        return data;
    }

    public void cleanData() {
        data.clear();
        notifyDataSetChanged();
    }

    public void refreshData(List<T> list) {
        if (list == null) {
            return;
        }
        data.clear();
        data.addAll(list);
        notifyDataSetChanged();
    }

    /**
     * @param list 向数据最后添加一段数据
     */
    public void addAllData(List<T> list) {
        addAllData(data.size(), list);
    }

    /**
     * 添加一段连续的数据
     *
     * @param startIndex 从此下标开始
     * @param list       添加的数据集合
     */
    public void addAllData(int startIndex, List<T> list) {
        if (list == null) {
            return;
        }
        data.addAll(startIndex, list);
        notifyItemRangeInserted(startIndex, list.size());
        notifyItemRangeChanged(startIndex, data.size() - startIndex);
    }

    public void addData(T bean) {
        addData(bean,data.size());
    }

    public void addData(T bean, int pos) {
        data.add(pos, bean);
        notifyItemInserted(pos);
        notifyItemRangeChanged(pos, data.size() - pos);
    }

    public void removeData(T bean) {
        int pos = data.indexOf(bean);
        if(pos<0){
            return;
        }
        removeData(pos);
    }

    public void removeData(int pos) {
        data.remove(pos);
        notifyItemRemoved(pos);
        notifyItemRangeChanged(pos, data.size() - pos);
    }

    /**
     * 移除一段连续的数据
     *
     * @param startIndex 从当前下标开始
     * @param count      移除的总长度
     */
    public void removeDataList(int startIndex, int count) {
        Iterator<T> iterator = data.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            T t = iterator.next();
            int index = data.indexOf(t);
            if (startIndex <= index && n < count) {
                n++;
                iterator.remove();
            }
        }
        notifyItemRangeRemoved(startIndex, count);
        notifyItemRangeChanged(startIndex, data.size() - startIndex);
    }

    public void notifyData(T bean) {
        notifyItemChanged(getData().indexOf(bean));
    }

    public static class BaseHolder extends RecyclerView.ViewHolder {
        private final ViewDataBinding dataBinding;

        public BaseHolder(ViewDataBinding itemView) {
            super(itemView.getRoot());
            this.dataBinding = itemView;
        }

        public <B extends ViewDataBinding> B getDataBinding() {
            return (B) dataBinding;
        }
    }

    public interface OnItemClick {
        void itemClick(View view, int position);
    }

    public interface OnItemLongClick {
        boolean itemClick(View view, int position);
    }

}
