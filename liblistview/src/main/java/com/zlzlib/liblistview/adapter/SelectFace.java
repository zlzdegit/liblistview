package com.zlzlib.liblistview.adapter;

/**
 * @Desc:
 * @Copyright:
 * @DateTime: 2022/4/2 17:11
 * @Author zlz
 * @Version 1.0
 */
public interface SelectFace {

    boolean isSelect();

    void setSelect(boolean select);

}
