package com.zlzlib.liblistview.adapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @Desc: 选择列表
 * @Copyright:
 * @DateTime: 2022/3/22 17:14
 * @Author zlz
 * @Version 1.0
 */
public abstract class SelectNormalAdapter<T extends SelectFace> extends BaseNormalAdapter<T> {

    private final List<T> selectList = new ArrayList<>();

    @Override
    public void refreshData(List<T> list) {
        super.refreshData(list);
        selectList.clear();
        for (T t : list) {
            if (t.isSelect()) {
                selectList.add(t);
            }
        }
    }

    /**
     * @param bean 单选选择的数据
     */
    public void selectSingleItem(T bean) {
        List<T> list = new ArrayList<>(1);
        list.add(bean);
        selectSingleItem(list);
    }

    /**
     * @param list 单选选择的列表
     */
    public void selectSingleItem(List<T> list) {
        for (T v : selectList) {
            v.setSelect(false);
        }
        selectList.clear();
        for (T v : list) {
            v.setSelect(true);
            selectList.add(v);
        }
        notifyDataSetChanged();
    }


    /**
     * @param pos 单选选择的下标
     */
    public void selectSingleItem(int pos) {
        selectSingleItem(getItem(pos));
    }

    /**
     * @param bean 多选选中的对象
     */
    public void selectItem(T bean) {
        List<T> list = new ArrayList<>(1);
        list.add(bean);
        selectItem(list);
    }

    /**
     * @param list 多选选中的列表
     */
    public void selectItem(List<T> list) {
        for (T t : list) {
            t.setSelect(!t.isSelect());
            if (t.isSelect()) {
                selectList.add(t);
            } else {
                selectList.remove(t);
            }
        }
        notifyDataSetChanged();
    }

    /**
     * @param pos 多选选中的下标
     */
    public void selectItem(int pos) {
        selectItem(getItem(pos));
    }

    /**
     * @return 当前选中的列表
     */
    public List<T> getSelectList() {
        return selectList;
    }

    /**
     * @return 当前单选选中的对象
     */
    public T getSelectSingle() {
        if (selectList.isEmpty()) {
            return null;
        }
        return selectList.get(0);
    }

}
