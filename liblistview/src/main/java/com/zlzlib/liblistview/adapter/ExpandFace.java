package com.zlzlib.liblistview.adapter;


/**
 * @Desc: 可展开列表接口
 * @Copyright:
 * @DateTime: 2022/6/17 11:07
 * @Author zlz
 * @Version 1.0
 */
public interface ExpandFace {

    boolean isExpand();

    void setExpand(boolean isExpand);
}
