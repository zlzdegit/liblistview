package com.zlzlib.liblistview.filter;

/**
 * @Desc:
 * @Copyright:
 * @DateTime: 2022/8/5 10:23
 * @Author zlz
 * @Version 1.0
 */
public interface BaseFilterFace {

    /**
     * @param key 匹配关键词
     * @return 是否匹配
     */
    boolean isContains(CharSequence key);

}
