package com.zlzlib.liblistviewtest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.zlzlib.liblistview.adapter.BaseRecAdapter;
import com.zlzlib.liblistviewtest.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private TestAdapter testAdapter;
    private int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_main, null, false);
        setContentView(binding.getRoot());
        binding.recycler.setLayoutManager(new LinearLayoutManager(this));
        binding.recycler.setAdapter(testAdapter = new TestAdapter());
        binding.btnAdd.setOnClickListener(v -> {
            index++;
            testAdapter.addData(new TestVo("名称" + index));
        });
        testAdapter.setItemClick((view, position) -> {
            testAdapter.selectItem(position);
        });
        testAdapter.setLongClick((view, position) -> {
            testAdapter.removeData(position);
            return true;
        });
    }


}