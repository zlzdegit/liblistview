package com.zlzlib.liblistviewtest;

import com.zlzlib.liblistview.adapter.SelectRecAdapter;

/**
 * @Desc:
 * @Copyright:
 * @DateTime: 2022/9/26 14:25
 * @Author zlz
 * @Version 1.0
 */
public class TestAdapter extends SelectRecAdapter<TestVo> {
    @Override
    protected int getLayoutId(int pos) {
        return R.layout.item_test;
    }

    @Override
    protected int getVariableId() {
        return BR.itemVo;
    }
}
