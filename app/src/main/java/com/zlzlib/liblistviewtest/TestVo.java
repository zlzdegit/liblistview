package com.zlzlib.liblistviewtest;

import com.zlzlib.liblistview.adapter.SelectBean;

/**
 * @Desc:
 * @Copyright:
 * @DateTime: 2022/9/26 14:25
 * @Author zlz
 * @Version 1.0
 */
public class TestVo extends SelectBean {


    private String name;

    public TestVo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String selectState() {
        return isSelect() ? "选中" : "未选中";
    }
}
